import java.util.*;

public class Produtomaximo {

    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        Scanner vt = new Scanner(System.in);

        int i;

        System.out.println("Informe o valor de n: ");
        int n = vt.nextInt();

        int v[] = new int[n];

        for (i = 0; i < v.length; i++) {
            System.out.printf("Informe %2do. valor de %d: ", (i + 1), n);
            v[i] = ler.nextInt();
        }
        System.out.println(calculaProduto(v));

    }

    public static int calculaProduto(int v[]) {
        int produto = 1;


        for (int i = 0; i < v.length; i++) {
            produto = produto * v[i];
        }
        return produto;

    }
}

