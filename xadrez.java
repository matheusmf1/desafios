import java.util.*;

public class xadrez {

	public static void main(String[] args) {
		String pedras[] = { "Peao", "Bispo", "Cavalo", "Torre", "Rei", "Rainha", "Vazio" };
		int pecas[] = new int[7];
		int[][] tabuleiro = { { 4, 3, 2, 5, 6, 2, 3, 4 }, 
							  { 1, 1, 1, 1, 1, 1, 1, 1 }, 
							  { 0, 0, 0, 0, 0, 0, 0, 0 },
							  { 0, 0, 0, 0, 0, 0, 0, 0 }, 
							  { 0, 0, 0, 0, 0, 0, 0, 0 }, 
							  { 0, 0, 0, 0, 0, 0, 0, 0 },
							  { 1, 1, 1, 1, 1, 1, 1, 1 }, 
							  { 4, 3, 2, 5, 6, 2, 3, 4 } };

		for (int i = 0; i < tabuleiro.length; i++) {
			for (int j = 0; j < tabuleiro.length; j++) {
				pecas[tabuleiro[i][j]]++;
			}
		}

		imprimirPecas(pecas);

	}

	private static void imprimirPecas(int[] pecas) {
		System.out.println("Peao: " + pecas[1] + " peca(s)");
		System.out.println("Bispo: " + pecas[2] + " peca(s)");
		System.out.println("Cavalo: " + pecas[4] + " peca(s)");
		System.out.println("Torre: " + pecas[3] + " peca(s)");
		System.out.println("Rainha: " + pecas[5] + " peca(s)");
		System.out.println("Rei: " + pecas[6] + " peca(s)");

	}

}
